package home.yoom.board;

import home.yoom.board.apps.board1.BoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

@Controller
public class CommonController {

    @Autowired
    BoardService boardService;

    @RequestMapping(value = "/image", method = RequestMethod.GET)
    public void getPic(String fileNo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String path = boardService.getImagePath(fileNo);
        File file = new File (path);
        FileInputStream fis = null;
        BufferedInputStream in = null;
        ByteArrayOutputStream bStream = null;
        try{
            fis = new FileInputStream(file);
            in = new BufferedInputStream(fis);
            bStream = new ByteArrayOutputStream();
            int imgByte;
            while ((imgByte = in.read()) != -1) {
                bStream.write(imgByte);
            }
//				response.setHeader("Content-Type", type);
            response.setContentLength(bStream.size());
            bStream.writeTo(response.getOutputStream());
            response.getOutputStream().flush();
            response.getOutputStream().close();
        }catch(Exception e){
        }finally{
            if (bStream != null) {
                try {
                    bStream.close();
                } catch (Exception est) {
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (Exception ei) {
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (Exception efis) {
                }
            }
        }

    }

}
