package home.yoom.board.domain;

public abstract class Backend {

    private int page = 1;                    // 페이지 번호
    private int pageSize = 10;                // 페이지 사이즈
    private int pageBlockSize = 5;            // 페이지 블럭 사이즈
    private int startContentsNum = 1; // 페이지시작
    private int endContentsNum = 10; // 페이지 끝

    private String searchField;                // 검색필드


    public int getStartContentsNum() {
        return startContentsNum;
    }

    public void setStartContentsNum(int startContentsNum) {
        this.startContentsNum = startContentsNum;
    }

    public int getEndContentsNum() {
        return endContentsNum;
    }

    public void setEndContentsNum(int endContentsNum) {
        this.endContentsNum = endContentsNum;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageBlockSize() {
        return pageBlockSize;
    }

    public void setPageBlockSize(int pageBlockSize) {
        this.pageBlockSize = pageBlockSize;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }
}
