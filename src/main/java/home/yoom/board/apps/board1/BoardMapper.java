package home.yoom.board.apps.board1;

import home.yoom.board.domain.BussinessCard;
import home.yoom.board.domain.FileMst;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BoardMapper {
	
	Integer insertCard(BussinessCard bussinessCard) throws Exception;
	List<BussinessCard> selectList(BussinessCard bussinessCard) throws Exception;
	Integer insertFile(FileMst fileMst) throws Exception;
	String selectImagePath(@Param("fileNo") int fileNo) throws Exception;
}
