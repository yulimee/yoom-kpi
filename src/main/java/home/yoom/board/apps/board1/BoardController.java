package home.yoom.board.apps.board1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import home.yoom.board.domain.BussinessCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("B/board")
public class BoardController {
	
	@Autowired
	private BoardServiceImpl boardService;
	
	@RequestMapping("list")
	public String BcList(HttpServletRequest request, Model model, BussinessCard bussinessCard) throws Exception {
		List<BussinessCard> list = boardService.selectList(bussinessCard);
		model.addAttribute("result", list);
		return "board/list";
	}

	@RequestMapping("detail")
	public String BcDetail (HttpServletRequest request, Model model, BussinessCard bussinessCard) throws Exception {

		return "board/detail";
	}

	@RequestMapping("/upload")
	@ResponseBody
	public String excelUpload (MultipartHttpServletRequest request, Model model) throws Exception {
		MultipartFile file = null;
		Iterator<String> iterator = request.getFileNames();
		if(iterator.hasNext()) {
			file = request.getFile(iterator.next());
		}
		Integer boardSeq = boardService.upload(file);

		return String.valueOf(boardSeq);
	}

	@RequestMapping("/callback")
	public void callback (HttpServletRequest request, @RequestBody List<String> parseList ) throws Exception {

		BussinessCard bc = new BussinessCard();

		bc.setName(parseList.get(1));
		bc.setPhone1(parseList.get(2));
		bc.setCompanyName(parseList.get(3));
		bc.setAddress(parseList.get(4));
		bc.setCompanyPhone(parseList.get(5));

		System.out.print("///////////////////////////callback 들어옴");
		System.out.print("///////////////////////////bc.name : " + bc.getName());
		System.out.print("///////////////////////////bc.getAddress : " + bc.getAddress());
		System.out.print("///////////////////////////bc.getCompanyName() : " + bc.getCompanyName());
		System.out.print("///////////////////////////bc.getCompanyPhone() : " + bc.getCompanyPhone());

		boardService.insertCard(bc);

	}

	@RequestMapping("saveBc")
	@ResponseBody
	public String saveBc(HttpServletRequest request, BussinessCard businessCard) {
		String resultCode = "0000"; // 성공

		try {
			boardService.insertCard(businessCard);
		} catch (Exception e) {
			e.printStackTrace();
			resultCode = "9999"; // 실패
		}

		return resultCode;
	}

	@RequestMapping("ocr")
	@ResponseBody
	public Map<String, Object> ocr(HttpServletRequest request, BussinessCard businessCard) {
		Map<String, Object> returnMap = new HashMap<>();

		returnMap.put("resultCode", "0000"); // 성공

		try {
			BussinessCard resultBc = boardService.googleOcr(businessCard);
			returnMap.put("resultData", resultBc);
		} catch (Exception e) {
			e.printStackTrace();
			returnMap.put("resultCode", "9999"); // 실패
		}


		return null;
	}

}