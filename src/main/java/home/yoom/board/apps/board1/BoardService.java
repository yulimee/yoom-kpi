package home.yoom.board.apps.board1;

import home.yoom.board.domain.BussinessCard;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface BoardService {

    void test(MultipartFile file) throws Exception;
    void insertCard(BussinessCard bussinessCard) throws Exception;
    Integer upload (MultipartFile file) throws Exception;
    String getImagePath(String fileNo) throws Exception;
    public void requestOcrApi(MultipartFile file) throws Exception;

    BussinessCard googleOcr(BussinessCard businessCard) throws Exception;
}
