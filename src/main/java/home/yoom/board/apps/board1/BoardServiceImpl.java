package home.yoom.board.apps.board1;

import com.google.cloud.vision.v1.*;
import com.google.cloud.vision.v1.Feature.Type;
import com.google.protobuf.ByteString;
import home.yoom.board.domain.BussinessCard;
import home.yoom.board.domain.FileMst;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class BoardServiceImpl implements BoardService{

    @Autowired
    private BoardMapper mapper;

	public List<BussinessCard> selectList(BussinessCard bussinessCard) throws Exception {
		return mapper.selectList(bussinessCard);
	}

	@Override
	public void test(MultipartFile file) throws Exception {
		// Instantiates a client
		try (ImageAnnotatorClient vision = ImageAnnotatorClient.create()) {

			// The path to the image file to annotate
			//String fileName = "C:\\Users\\Taemin\\Documents\\카카오톡 받은 파일\\zz.jpg";
			String fileName = file.getName();

			// Reads the image file into memory
			Path path = Paths.get(fileName);
			byte[] data = Files.readAllBytes(path);
			ByteString imgBytes = ByteString.copyFrom(data);

			// Builds the image annotation request
			List<AnnotateImageRequest> requests = new ArrayList<>();
			Image img = Image.newBuilder().setContent(imgBytes).build();
			Feature feat = Feature.newBuilder().setType(Type.LABEL_DETECTION).build();
			AnnotateImageRequest request = AnnotateImageRequest.newBuilder()
					.addFeatures(feat)
					.setImage(img)
					.build();
			requests.add(request);

			// Performs label detection on the image file
			BatchAnnotateImagesResponse response = vision.batchAnnotateImages(requests);
			List<AnnotateImageResponse> responses = response.getResponsesList();

			for (AnnotateImageResponse res : responses) {
				if (res.hasError()) {
					System.out.printf("Error: %s\n", res.getError().getMessage());
					return;
				}

//				for (EntityAnnotation annotation : res.getLabelAnnotationsList()) {
//					annotation.getAllFields().forEach((k, v) ->
//							System.out.printf("%s : %s\n", k, v.toString()));
//				}

				for (EntityAnnotation annotation : res.getTextAnnotationsList()) {
					annotation.getDescription();
				}

			}
		}
	}

	@Override
	public void insertCard(BussinessCard bussinessCard) throws Exception {

        mapper.insertCard(bussinessCard);
	}

	@Override
	public void requestOcrApi(MultipartFile file) throws Exception {
		// Instantiates a client
		try (ImageAnnotatorClient vision = ImageAnnotatorClient.create()) {

			String fileName = file.getName();

			Path path = Paths.get(fileName);
			byte[] data = Files.readAllBytes(path);
			ByteString imgBytes = ByteString.copyFrom(data);

			//String gcsSourcePath, String gcsDestinationPath
//			OcrUtil.detectDocumentsGcs(file.getOriginalFilename(),"B/board/callback");
		}
	}

	@Override
	public Integer upload(MultipartFile file) throws Exception {

		String prePath = "D:\\temp\\";
		String fullFilePath = null;

		OutputStream out = null;

		UUID one = UUID.randomUUID();
		String fileNameUUID = one.toString();

		String fileName = file.getOriginalFilename();
		String ext = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());

		try {
			byte[] bytes = file.getBytes();
			fullFilePath = prePath + fileNameUUID + "." + ext;
			File newFile = new File(fullFilePath);

			out = new FileOutputStream(newFile);
			out.write(bytes);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (out != null) {
				out.close();
			}
		}

		FileMst fileMst = new FileMst();
		fileMst.setSaveName(fullFilePath);

		mapper.insertFile(fileMst);

		return fileMst.getNo();
	}

	@Override
	public String getImagePath(String fileNo) throws Exception {
		return mapper.selectImagePath(Integer.parseInt(fileNo));
	}

	@Override
	public BussinessCard googleOcr(BussinessCard businessCard) throws Exception {
		BussinessCard returnBc = null;
		String filePath = mapper.selectImagePath(Integer.parseInt(businessCard.getFileNo()));

		if (StringUtils.isNotEmpty(filePath)) {
			returnBc = ocrProcess(filePath);
		} else {
			throw new Exception("not exsist file path");
		}

		return returnBc;
	}

	/**
	 * get 명함 텍스트 OCR
	 * @param filePath
	 * @return
	 * @throws Exception
	 */
	private BussinessCard ocrProcess (String filePath) throws Exception {
		BussinessCard returnBc = new BussinessCard();

		try (ImageAnnotatorClient vision = ImageAnnotatorClient.create()) {

			Path path = Paths.get(filePath);
			byte[] data = Files.readAllBytes(path);
			ByteString imgBytes = ByteString.copyFrom(data);

			List<AnnotateImageRequest> requests = new ArrayList<>();
			Image img = Image.newBuilder().setContent(imgBytes).build();
			Feature feat = Feature.newBuilder().setType(Type.LABEL_DETECTION).build();
			AnnotateImageRequest request = AnnotateImageRequest.newBuilder()
					.addFeatures(feat)
					.setImage(img)
					.build();
			requests.add(request);

			BatchAnnotateImagesResponse response = vision.batchAnnotateImages(requests);
			List<AnnotateImageResponse> responses = response.getResponsesList();

			for (AnnotateImageResponse res : responses) {
				if (res.hasError()) {
					System.out.printf("Error: %s\n", res.getError().getMessage());
					throw new Exception();
				}

//				for (EntityAnnotation annotation : res.getLabelAnnotationsList()) {
//					annotation.getAllFields().forEach((k, v) ->
//							System.out.printf("%s : %s\n", k, v.toString()));
//				}

				int idx = 0;
				List<String> etcDescList = new ArrayList<String>();
				for (EntityAnnotation annotation : res.getTextAnnotationsList()) {
					String desc = annotation.getDescription();
					if (StringUtils.isNotEmpty(desc)) {
						switch (idx) {
							case 0:
								returnBc.setName(desc);
								break;
							case 1:
								returnBc.setAddress(desc);
								break;
							case 2:
								returnBc.setPhone1(desc);
								break;
							case 3:
								returnBc.setCompanyName(desc);
								break;
							case 4:
								returnBc.setCompanyPhone(desc);
								break;
							default:
								etcDescList.add(desc);
						}
						if (etcDescList.size() != 0) {
							returnBc.setEtcDescList(etcDescList);
						}
					}
					idx++;
				}

			}

		}

		return returnBc;
	}

}
