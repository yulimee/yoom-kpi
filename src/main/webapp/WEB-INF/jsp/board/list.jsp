<%@ page contentType="text/html;charset=UTF-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="content">
    <form id="pageForm">
        <hidden name="page" value="${page}"/>
    </form>
    <div class="row" style="padding-top:11px">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">DataTables Advanced Tables</div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="dataTables-example_wrapper"
                         class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="dataTables_length" id="dataTables-example_length">
                                    <label>Show <select name="dataTables-example_length"
                                                        aria-controls="dataTables-example"
                                                        class="form-control input-sm">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select> entries
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div id="dataTables-example_filter" class="dataTables_filter">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">명함업로드</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table width="100%"
                                       class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline"
                                       id="dataTables-example" role="grid"
                                       aria-describedby="dataTables-example_info" style="width: 100%;">
                                    <colgroup>
                                        <col width="15%"/>
                                        <col width="15%"/>
                                        <col width="15%"/>
                                        <col width="15%"/>
                                        <col width="20%"/>
                                        <col width="20%"/>
                                    </colgroup>
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0"
                                            aria-controls="dataTables-example" rowspan="1" colspan="1"
                                            aria-sort="ascending"
                                            aria-label="Rendering engine: activate to sort column descending"
                                            style="width: 83px;">이름
                                        </th>
                                        <th class="sorting" tabindex="0"
                                            aria-controls="dataTables-example" rowspan="1" colspan="1"
                                            aria-label="Browser: activate to sort column ascending"
                                            style="width: 105px;">주소
                                        </th>
                                        <th class="sorting" tabindex="0"
                                            aria-controls="dataTables-example" rowspan="1" colspan="1"
                                            aria-label="Platform(s): activate to sort column ascending"
                                            style="width: 95px;">전화번호
                                        </th>
                                        <th class="sorting" tabindex="0"
                                            aria-controls="dataTables-example" rowspan="1" colspan="1"
                                            aria-label="Engine version: activate to sort column ascending"
                                            style="width: 71px;">회사이름
                                        </th>
                                        <th class="sorting" tabindex="0"
                                            aria-controls="dataTables-example" rowspan="1" colspan="1"
                                            aria-label="CSS grade: activate to sort column ascending"
                                            style="width: 50px;">회사전화번호
                                        </th>
                                        <th class="sorting" tabindex="0"
                                            aria-controls="dataTables-example" rowspan="1" colspan="1"
                                            aria-label="CSS grade: activate to sort column ascending"
                                            style="width: 50px;">등록일
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${result}" var="item" varStatus="status">
                                        <tr class="gradeA odd" role="row">
                                            <td class="center"><a href="#bcDetail" data-toggle="modal" data-memo="${item.memo}" data-fileNo="${item.fileNo}" onclick="flow.action.setDetail(this);"><c:out value="${item.name}"/></a></td>
                                            <td class="center"><c:out value="${item.address}"/></td>
                                            <td class="center"><c:out value="${item.phone1}"/></td>
                                            <td class="center"><c:out value="${item.companyName}"/></td>
                                            <td class="center"><c:out value="${item.companyPhone}"/></td>
                                            <td class="center"><c:out value="${item.regDate}"/></td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="dataTables_info" id="dataTables-example_info"
                                     role="status" aria-live="polite">Showing 1 to 10 of 57
                                    entries
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="dataTables_paginate paging_simple_numbers"
                                     id="dataTables-example_paginate">
                                    <ul class="pagination">
                                        <li class="paginate_button previous disabled"
                                            aria-controls="dataTables-example" tabindex="0"
                                            id="dataTables-example_previous"><a href="#">Previous</a></li>
                                        <li class="paginate_button active"
                                            aria-controls="dataTables-example" tabindex="0"><a
                                                href="#">1</a></li>
                                        <li class="paginate_button "
                                            aria-controls="dataTables-example" tabindex="0"><a
                                                href="#">2</a></li>
                                        <li class="paginate_button "
                                            aria-controls="dataTables-example" tabindex="0"><a
                                                href="#">3</a></li>
                                        <li class="paginate_button "
                                            aria-controls="dataTables-example" tabindex="0"><a
                                                href="#">4</a></li>
                                        <li class="paginate_button "
                                            aria-controls="dataTables-example" tabindex="0"><a
                                                href="#">5</a></li>
                                        <li class="paginate_button next"
                                            aria-controls="dataTables-example" tabindex="0"
                                            id="dataTables-example_next"><a href="#">Next</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">주소록 등록</h4>
				</div>
				<div class="modal-body">

                    <label class="checkbox-inline">
                        <input type="checkbox" id="checkUpload"> 명함 업로드
                    </label>
                    <br>
                    <form id="fileUpForm">
                        <input type="file" id="file" name="file">
                        <input type="hidden" id="fileNo" name="fileNo" />
                    </form>

                    <span id="imgField">
                        <img src="" id="imgView" style="max-width:200px;">
                    </span>
                    <br><br>

                    <div class="form-group">
                        <label class="control-label" for="name">이름* </label>
                        <input type="text" class="form-control" id="name">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="address">주소* </label>
                        <input type="text" class="form-control" id="address">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="phone1">연락처1* </label>
                        <input type="text" class="form-control" id="phone1">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="companyName">회사명* </label>
                        <input type="text" class="form-control" id="companyName">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="companyPhone">회사연락처* </label>
                        <input type="text" class="form-control" id="companyPhone">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="memo">메모 </label>
                        <input type="text" class="form-control" id="memo">
                    </div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
                    <button type="button" class="btn btn-primary" id="submitBtn">등록</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
    <div class="modal fade" id="bcDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <span>
                        <img src="" id="detailImg" style="max-width:200px;">
                    </span>
                    <br><br>

                    <div class="form-group">
                        <label class="control-label" for="dtName">이름* </label>
                        <input type="text" class="form-control" id="dtName" readonly="readonly">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="dtAddress">주소* </label>
                        <input type="text" class="form-control" id="dtAddress" readonly="readonly">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="dtPhone1">연락처1* </label>
                        <input type="text" class="form-control" id="dtPhone1" readonly="readonly">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="dtCompanyName">회사명* </label>
                        <input type="text" class="form-control" id="dtCompanyName" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="dtCompanyPhone">회사연락처* </label>
                        <input type="text" class="form-control" id="dtCompanyPhone" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="dtMemo">메모 </label>
                        <input type="text" class="form-control" id="dtMemo" readonly="readonly">
                    </div>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<!-- / content -->

<div id="scriptPost">
    <script>
        $(document).ready(function () {
            flow.init();
            $("#fileUpForm").hide();
        });

        var flow = {
            init: function () {
                this.bind();
            }
            , bind : function () {

                $("#submitBtn").on("click", function () {
                    flow.action.save();
                });

                //텍스트필드 입력값 체크
                $("input.form-control").on('blur', function (){
                    if($(this).val() != "" && $(this).val() != null){
                        $(this).parent().attr('class',"form-group has-success");

                    } else{
                        $(this).parent().attr('class',"form-group has-error");
                        $(this).attr('placeholder','내용을 입력하세요');

                    }
                });

                //명함등록 체크박스 체크시
                $("input[type=checkbox]").on('click', function (){
                    console.log("이거 :"+ $(this).prop('checked'));
                    var checkFlag = $(this).prop('checked');
                    //체크시
                    if(checkFlag){
                        $(".form-group").find('input').prop('disabled','true');
                        $("#fileUpForm").show();
                    //체크 해제시
                    }else{
                        $(".form-group").find('input').removeAttr('disabled');
                        $("#file").val('');
                        $("#imgView").attr('src','');
                        $("#fileUpForm").hide();
                    }
                });

                //파일 첨부시 미리보기
                $("input[type=file]").on('change', function (value){
                    console.log('파일첨부했쪄용');

                    if(this.files && this.files[0]){
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $("#imgView").attr('src', e.target.result);
                        }
                        reader.readAsDataURL((this.files[0]));
                        if(!$("#checkUpload").prop('checked')){
                            $("#checkUpload").trigger('click');
                        }

                    } else {
                        $("#fileNo").clear();
                    }

                    var form = $("#fileUpForm")[0];
                    var data = new FormData(form);
                    $.ajax({
                        enctype:"multipart/form-data",
                        method:"POST",
                        url: 'upload',
                        processData: false,
                        contentType: false,
                        cache: false,
                        data: data,
                        success: function(result){
                            if (result != null) {
                                console.log(result);
                                $("#fileNo").val(result);
                                $(".form-group").find('input').removeAttr('disabled');
                            } else {
                                $("#fileNo").clear();
                                alert("이미지 등록에 실패했습니다. 겸댕이 유림이에게 문의하세요");
                            }
                        }
                    });
                });
            }
            , action : {
                save : function () {

                    if (!$("#name").val()) {
                        alert("필수값을 입력하세요.")
                        return;
                    }
                    if (!$("#address").val()) {
                        alert("필수값을 입력하세요.")
                        return;
                    }
                    if (!$("#phone1").val()) {
                        alert("필수값을 입력하세요.")
                        return;
                    }
                    if (!$("#companyName").val()) {
                        alert("필수값을 입력하세요.")
                        return;
                    }
                    if (!$("#companyPhone").val()) {
                        alert("필수값을 입력하세요.")
                        return;
                    }

                    var paramData = {
                        "name" : $("#name").val()
                        , "address" : $("#address").val()
                        , "phone1" : $("#phone1").val()
                        , "companyName" : $("#companyName").val()
                        , "companyPhone" : $("#companyPhone").val()
                        , "memo" : $("#memo").val()
                        , "fileNo" : $("#fileNo").val()
                    }

                    $.ajax({
                        url: 'saveBc',
                        type:'post',
                        cache: false,
                        dataType : "text",
                        data: paramData,
                        success: function(result){
                            if (result == "0000") {
                                alert("등록 성공");
                                $(".close").trigger("click");
                            } else {
                                alert("등록 실패");
                            }
                        }
                    })

                }, setDetail: function (data) {
                    $("#dtMemo").val($(data).attr("data-memo"));

                    if ($(data).attr("data-fileNo")) {
                        $("#detailImg").attr("src", "/image?fileNo=" + $(data).attr("data-fileNo"));
                    } else {
                        $("#detailImg").attr("src", "");
                    }


                    console.log("파일넘버" + $(data).attr("data-fileNo"));

                    $.each($(data).parent().parent().find("td"), function (idx, item) {
                        switch(idx) {
                            case 0:
                                $("#dtName").val($(item).text());
                                break;
                            case 1:
                                $("#dtAddress").val($(item).text());
                                break;
                            case 2:
                                $("#dtPhone1").val($(item).text());
                                break;
                            case 3:
                                $("#dtCompanyName").val($(item).text());
                                break;
                            case 4:
                                $("#dtCompanyPhone").val($(item).text());
                                break;

                        }



                    });



                }
            }
        }
    </script>
</div>